def computegrade(score):
	if score < 0 or score > 1:
		print("Score is out of range")
	elif score >= 0.9:
		return "Your grade is A"
	elif score >= 0.8:
		return "Your grade is B"
	elif score >= 0.7:
		return "Your grade is C"
	elif score >= 0.6:
		return "Your grade is D"
	elif score < 0.6:
		return "Your grade is F"

try:
	score = float(input("What was your score? "))
except:
	print("Bad score")
	quit()

print(computegrade(score))
