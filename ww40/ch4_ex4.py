# Exercise 4: What is the purpose of the "def" keyword in Python?

# a) It is slang that means "the following code is really cool"
# b) It indicates the start of a function
# c) It indicates that the following indented section of code is to be stored for later
# d) b and c are both true
# e) None of the above

# Answer = I would argue that the answer is b, as the function is not stored, but rather executed when it is called.