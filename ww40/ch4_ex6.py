# Exercise 6: Rewrite your pay computation with time-and-a-half for overtime and create a function called computepay which takes two parameters (hours and rate).

def computepay(hours, rate):
	if (hours) > 40:
		overtime = (hours) - 40
		overtimepay = (rate) * 1.5
		pay = (hours - overtime) * rate + overtime * overtimepay
	else:
		pay = hours * rate
	return pay

hours = float(input("Please enter how many hours you have worked: "))
rate = float(input("Please input your hourly rate: "))
pay = computepay(hours, rate)

print(pay)