"""Chapter 11 Exercises, week 48
Exercise 2
20A-ITT1-PROGRAMMING w/ NISI"""

#Exercise 2, see page 145 (PDF) 
#Exercise 2: Write a program to look for lines of the form:
#    New Revision: 39772
#Extract the number from each of the lines using a regular expression
#and the findall() method. Compute the average of the numbers and
#print out the average as an integer.
import re

#for line in text:
#    line = line.strip()     #seperates each line
#    l = re.findall('^New .+ [0-9]', line)
#    if len(l) > 0:
#        print(l)
count = 0
total = 0
hand = open('mbox-short.txt')
for line in hand:
    line = line.rstrip()
    x = re.findall('^New Revision: ([0-9]+)', line)
    value = x[0]
    value = int(value)
    if len(value) > 0:
        count += 1
        total += value

print(total/count)