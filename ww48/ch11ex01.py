"""Chapter 11 Exercises, week 48
Exercise 1
20A-ITT1-PROGRAMMING w/ NISI"""

#Exercise 1, see page 145 (PDF) 
#Exercise 1: Write a simple program to simulate the operation of the
#grep command on Unix. Ask the user to enter a regular expression and
#count the number of lines that matched the regular expression:
import re

question = input("Enter a regular expression: ")
text = 'mbox.txt'
data_list = [line.strip('\n') 
             for line in open
             (text, 'r')]

pattern = re.compile(question)

count = 0
for line in data_list:
    result = re.search(pattern, line)
    if result: count += 1

print ("%s had %d lines that matched %s" % (text, count, question))