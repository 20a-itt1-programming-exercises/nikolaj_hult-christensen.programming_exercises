# Exercise 0
1. What the Python interpreter is and how it differs from a compiler?  
Compiler translates the programing language into machine code. An interpreter runs the program directly in the language it was written in. 
2. What is the difference between syntax errors, logic errors and semantic errors?  
Syntax = Grammar, Logic = mistake in the programs logic, semantic = problem without producing error, but doesnt do the right thing.
3. What is a program?  
A program is a series of instructions, executed by the CPU  
4. What is input and output?  
Input is external influence to the computer, output is what the computer puts out  
5. What is sequential execution?  
Each command is executed in the order they are listed  
6. Which 4 things should you do when debugging?  
Read, run, ruminate, retreat. Also see the five stages of grief (Denial, anger, bargain, depression, acceptance)
