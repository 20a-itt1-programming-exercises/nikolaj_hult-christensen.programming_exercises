#Exercise 4, see page 116 (PDF)
#Exercise 4: Add code to the above program to figure out who has the most messages in the file. 
#After all the data has been read and the dictionary has been created, look through the dictionary using a maximum
#loop (see Chapter 5: Maximum and minimum loops) to find who has the most messages and print how many messages the person has.

doc = open("mbox-short.txt") # opens document
mails = dict() # creates dictionary called mails
for line in doc:
    line = line.rstrip() # strips right side of line
    if not line.startswith("From "): continue # if line does not start with "From "
    word = line.split() # Makes a list containing the words in a line
    if len(word) == 0 : continue    # if line is empty, continue
    if "@" not in word[1]: continue # if word number 2 does not contain a "@", continue
    mail = word[1] # defines variable "mail" and word number 2
    if mail not in mails: # if word number 2 (the email address) is not in the dictionary
        mails[mail] = 1 # add the email address to the dictionary with the value of 1
    else:
        mails[mail] += 1 # add one more to the counter for the email in the dictionary

#Counting amounts
largest = None
for counting in mails:                                  #Starting max loop
    if largest is None or counting > largest:           #checks whether or not counting is bigger than largest
        largest = counting                              #if it is, it updates the variable with the new number
print("Biggest fuckboy = ", largest)
print("Sneaked into this many DM's = ", mails[largest])