#Exercise 5, see page 115 (PDF)
#This program records the domain name (instead of the address) where the message was sent from instead of who the mail came
#from (i.e., the whole email address). At the end of the program, print out the contents of your dictionary.

doc = open("mbox-short.txt")                    # opens document
mails = dict()                                  # creates dictionary called mails
for line in doc:
    line = line.rstrip()                        # strips right side of line
    if not line.startswith("From "): continue   # if line does not start with "From"  
    word = line.split()                         # Makes a list containing the words in a line
    if len(word) == 0 : continue                # if line is empty, continue
    if "@" not in word[1]: continue             # if word number 2 does not contain a "@", continue
    mail = word[1]                              # defines variable "mail" and word number 2 AKA removes "from"
    if mail not in mails:                       # if word number 2 (the email address) is not in the dictionary
        mails[mail] = 1                         # add the email address to the dictionary with the value of 1
    else:
        mails[mail] += 1                        # add one more to the counter for the email in the dictionary

domain = list(mails.keys())
for key in domain:
    key = key.split("@")
    print(key[1])

#strips left for @ to show domain name only