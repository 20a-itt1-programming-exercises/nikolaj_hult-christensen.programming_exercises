 #Write a program that categorizes each mail message by which day of the week the commit was done.
 #To do this look for lines that start with "From", then look for the third word and keep a running count of each of the days of the week. 
 #At the end of the program print out the contents of your dictionary (order does not matter).

 #Sample Line:

#    From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008

#Sample Execution:

#	python dow.py
#	Enter a file name: mbox-short.txt
#	{'Fri': 20, 'Thu': 6, 'Sat': 1}

#fileName = input("Please enter the file you would like to open: ")
#try:
#    fileHandle = open(fileName)
#except:
#    print("File cannot be opened")
#    exit()

#open me daddy
doc = open("mbox-short.txt")
calendar = dict()
for line in doc:
    line = line.rstrip()
    if not line.startswith("From"): continue
    words = line.split()
    if len(words) == 0 : continue
    if len(words) < 3 : continue
    day = words[2]
    if day not in calendar:
        calendar[day] = 1
    else:
        calendar[day] += 1

print(calendar)