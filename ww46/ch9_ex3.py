 # Write a program to read through a mail log, build a histogram using a dictionary to count how many messages have come from each email address, and print the dictionary.

#   Enter file name: mbox-short.txt
#   {'gopal.ramasammycook@gmail.com': 1, 'louis@media.berkeley.edu': 3,
#   'cwen@iupui.edu': 5, 'antranig@caret.cam.ac.uk': 1,
#   'rjlowe@iupui.edu': 2, 'gsilver@umich.edu': 3,
#   'david.horwitz@uct.ac.za': 4, 'wagnermr@iupui.edu': 1,
#   'zqian@umich.edu': 4, 'stephen.marquard@uct.ac.za': 2,
#   'ray@media.berkeley.edu': 1}

doc = open("mbox-short.txt") # opens document
mails = dict() # creates dictionary called mails
for line in doc:
    line = line.rstrip() # strips right side of line
    if not line.startswith("From "): continue # if line does not start with "From"
    word = line.split() # Makes a list containing the words in a line
    if len(word) == 0 : continue    # if line is empty, continue
    if "@" not in word[1]: continue # if word number 2 does not contain a "@", continue
    mail = word[1] # defines variable "mail" and word number 2
    if mail not in mails: # if word number 2 (the email address) is not in the dictionary
        mails[mail] = 1 # add the email address to the dictionary with the value of 1
    else:
        mails[mail] += 1 # add one more to the counter for the email in the dictionary

print(mails)