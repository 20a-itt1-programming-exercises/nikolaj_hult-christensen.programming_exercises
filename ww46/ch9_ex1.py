#Write a program that reads the words in words.txt and stores them as keys in a dictionary. It doesn't matter what the values are. Then you can use the in operator as a fast way to check whether a string is in the dictionary.

import string

fileName = input("Enter the file name: ")
try:
    fileHandle = open(fileName)
except:
    print("File cannot be opened")
    exit()

counts = dict()
for line in fileHandle:
    line = line.rstrip()
    line = line.translate(line.maketrans("", "", string.punctuation)) # ignores punctuation
    line = line.lower()
    words = line.split()
    for word in words:
        if word not in counts:
            counts[word] = 1
        else:
            counts[word] += 1
while True:
    wordFind = input("Enter the word you would like to find: ")
    wordFindLow = str.lower(wordFind)
    print(wordFindLow in counts)
