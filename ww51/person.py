from datetime import datetime

# parent class
class Person:

	# Constructor/init method
	def __init__(self, fullname, birthday, address, gender):
		self.fullname = fullname
		self.birthday = birthday
		self.address = address
		self.gender = gender

	def get_first_name(self):
		return self.fullname.split(' ')[0]

	def get_last_name(self):
		return self.fullname.split(' ')[1]

	def get_age(self):   # make me work
		today = datetime.today()
		ageInDays = (today - self.birthday).days
		ageInYears = ageInDays / 365
		return int(ageInYears)

	def get_address(self):
		return self.address[:3] # only return list(streetname, streetnumber, city)
	
	def get_full_info(self):
		return self.get_first_name(), self.get_last_name(), self.get_age(), self.get_address()

# child class
class Extended_Person(Person):

	def __init__(self, fullname, birthday, address, gender, pet_type, pet_name, monthly_income):
		Person.__init__(self, fullname, birthday, address, gender)
		self.pet_type = pet_type
		self.pet_name = pet_name
		self.monthly_income = monthly_income

	def get_pet(self):
		return self.pet_type + " named " + self.pet_name

	def get_income(self):
		self.monthly_income = int(self.monthly_income)
		if self.monthly_income < 10000:
			return "income: low"
		elif self.monthly_income > 20000:
			return "income: high"
		else:
			return "income: middle"

def main():
 #testing the classes
	person1 = Person("Anders Andersen", datetime(1901, 1, 1), ("Avej", "1", "Astrup", "1111", "DK"), "male")
	person2 = Person("Bente bent", datetime(2001, 2, 12), ("Byvej", "2", "Bullerup", "2222", "DK"), "male")
	person3 = Person("Catja Caj", datetime(2001, 3, 23), ("Cykelvej", "3", "Castrup", "3333", "DK"), "male")

	print(person1.get_full_info())
	print(person2.get_full_info())
	print(person3.get_full_info())


	person1 = Extended_Person("Anders Andersen", datetime(1901, 1, 1), ("Avej", "1", "Astrup", "1111", "DK"), "male", "dog", "doggy", "42069")
	person2 = Extended_Person("Bente bent", datetime(2001, 2, 12), ("Byvej", "2", "Bullerup", "2222", "DK"), "male", "llama", "Dali llama", "20")
	person3 = Extended_Person("Catja Caj", datetime(2001, 3, 23), ("Cykelvej", "3", "Castrup", "3333", "DK"), "male", "Albino death-bullfrog", "casper", "99999999")

	print(person1.get_pet(), person1.get_income())
	print(person2.get_pet(), person2.get_income())
	print(person3.get_pet(), person3.get_income())
	
if __name__ == "__main__":
	main()