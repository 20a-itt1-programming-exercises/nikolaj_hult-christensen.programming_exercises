# Use pair programming https://resources.collab.net/agile-101/pair-programming to write a Python application that can read an RSS feed from gitlab.

import feedparser
import string
from bs4 import BeautifulSoup

token = # ADD token/text file here
eal_itt = "https://gitlab.com/EAL-ITT.atom?feed_token=oqLxLiL8aGLk9rVyLN2G" # remove token and put in text file
rss_feed = feedparser.parse(eal_itt)
counter = 0

for _ in range(5): #do it 5 times
    print(rss_feed.entries[counter].author)
    print(rss_feed.entries[counter].updated)
    commit_source = rss_feed.entries[counter].summary           #gives commit_source the content of 'summary', once per counter
    soup = BeautifulSoup(commit_source, 'html.parser')
    commit = soup.find('div', class_='blockquote').get_text()   #search of div's with named classes
    print(commit+"\n")
    counter +=1