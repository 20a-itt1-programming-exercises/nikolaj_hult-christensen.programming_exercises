import random
import time

computerWin = 0
humanWin = 0
game = 1
while game == 1:
	attempts = 3
	randNum = random.randint(0,9)
	while attempts > 0:
		guess = input("Guess a number between 0 and 9.")
		try:
			guess = int(guess)
		except ValueError:
			print("Thats not even a number.... retarting")
			continue
		if guess < 0:
			print("You could not be more wrong, you literally had to guess between 0 and 9, and you couldnt even do that")
			continue
		elif guess > 9:
			print("You could not be more wrong, you literally had to guess between 0 and 9, and you couldnt even do that")
			continue
		elif guess == randNum:
			correct = True
			while correct == True:
				tryAgain = str.lower(input("Damn, you won!, try again? (y/n)"))
				if tryAgain == "y":
					humanWin = humanWin + 1
					correct = False
					attempts = 0
				elif tryAgain == "n":
					humanWin = humanWin +1
					correct = False
					attempts = 0
					game = 0
					print("The score is: Computer has %d wins and you have %d wins" % (computerWin, humanWin))
				else:
					print("you are retarded")
			continue
		elif attempts > 1:
			attempts = attempts - 1
			print("Your guess was wrong, you have %d attempts left." % attempts)
			continue
		else:
			print("You are out of attempts, loser. I win!")
			doOver = True
			while doOver == True:
				tryAgain = str.lower(input("Damn, you won!, try again? (y/n)"))
				if tryAgain == "y":
					computerWin = computerWin + 1
					doOver = False
					attempts = 0
				elif tryAgain == "n":
					computerWin = computerWin + 1
					doOver = False
					attempts = 0
					game = 0
				else:
					print("you are retarded")
			continue
		
print("Stopping the game...")