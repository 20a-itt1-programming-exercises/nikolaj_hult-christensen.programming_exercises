# Exercise 2: Rewrite your pay program using try and except so that your program handles non-numeric input gracefully by printing a message and exiting the program. The following shows two executions of the program
while True:
	try:
		hours = float(input("Please enter how many hours you have worked: "))
		hourlyRate = float(input("Please input your hourly rate: "))
		if (hours) > 40:
			overtime = (hours) - 40
			overtimepay = (hourlyRate) * 1.5
			pay = (hours - overtime) * hourlyRate + overtime * overtimepay
			print(pay)
		else:
			pay = hours * hourlyRate
			print(pay)
		break
	except:
		print("Please enter only numbers")