# Exercise 1: Rewrite your pay computation to give the employee 1.5 times the hourly rate for hours worked above 40 hours.
hours = float(input("Please enter how many hours you have worked: "))
hourlyRate = float(input("Please input your hourly rate: "))
if (hours) > 40:
	overtime = (hours) - 40
	overtimepay = (hourlyRate) * 1.5
	pay = (hours - overtime) * hourlyRate + overtime * overtimepay
	print(pay)
else:
	pay = hours * hourlyRate
	print(pay)