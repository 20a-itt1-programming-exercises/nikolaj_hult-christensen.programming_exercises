# Exercise 3: Write a program to prompt for a score between 0.0 and 1.0. If the score is out of range, print an error message. If the score is between 0.0 and 1.0, print a grade using the following table
while True:
	try:
		gradetable= "ABCDF"
		score = float(input("What was your score? "))
		if score <= 1 and score >= 0:
			if score >= 0.9:
				print("Your grade is " + gradetable[0])
			elif score >= 0.8:
				print("Your grade is " + gradetable[1])
			elif score >= 0.7:
				print("Your grade is " + gradetable[2])
			elif score >= 0.6:
				print("Your grade is " + gradetable[3])
			elif score < 0.6:
				print("Your grade is " + gradetable[4])
		else:
			print("Please enter a valid number between 0 and 1")
			continue
		break
	except:
		print("Please enter a valid number between 0 and 1")