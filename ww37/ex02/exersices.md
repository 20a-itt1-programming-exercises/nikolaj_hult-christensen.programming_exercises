# 1.E: introduction (Exercises)

1. What is the function of the secondary memory in a computer?  
	 a. C
2. What is a program?
     a. A program is a series of instructions, executed by the CPU
3. What is the difference between a compiler and an interpreter?
     a. A compiler translates the programing language into machine code. An interpreter runs the program directly in the language it was written in.
4. Which of the following contains "machine code"?
     a. A  
5. What is wrong with the following code:
> primt 'Hello world!'  
      File "<stdin>", line 1  
        primt 'Hello world!'  
--------------------^  
    SyntaxError: invalid syntax

  i. "print" is spelled wrong, and the function is missing parentheses around the argument
6. Where in the computer is a variable such as "x" stored after the following Python line finishes?
> x = 123

   i. Main Memory
7. What will the following program print out:  
>x = 43  
x = x + 1  
print(x)  

   i. 44
8. Explain each of the following using an example of a human capability: (1) Central processing unit, (2) Main Memory, (3) Secondary Memory, (4) Input Device, and (5) Output Device. For example, "What is the human equivalent to a Central Processing Unit"?  
    a. (1) Brain (2) short term memory (3) long term memory (4) Mouth (5) also mouth
9. How do you fix a "Syntax Error"  
   a. Syntax errors are grammar mistakes in the programming language, go fix your grammar mistakes